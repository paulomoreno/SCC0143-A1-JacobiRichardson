Integrantes
-------------
Matheus D. Compri 	- 7151885
Paulo H. O. Moreno	- 7704943

Compilação
-------------
Para compilar o trabalho, basta executar o arquivo compile.sh. Será gerado o arquivo executavel a1openMP

Execução
-------------
Para executa-los basta digitar ./a1openMP filename, onde filename é o nome da matriz a ser passada como entrada.