#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

/*
   Estrutura de dados para armazenar a matriz
 */
typedef struct {
	double **matrix;
	int rows;
	int columns;
} matrix;

/*
   Estrutura para armazenar parametros de entrada
 */
int J_ORDER;
int J_ROW_TEST;
double J_ERROR;
int J_IT_MAX;

// Valores de x, x anterior, erro
double *x, *xp, *error;

// Matrizes 
matrix mA;
matrix mB;

// Variavel q armazena o tempo gasto
double elapsed = 0;

/*
   Funcao para alocar uma matriz
 */
void allocateMatrix(matrix *m1, int columns, int rows){
	// Indices para percorrer a matriz
	int i, j;

	// Salvando os valores de colunas e linhas
	m1->columns = columns;
	m1->rows = rows;

	// Alocando as linhas e as colunas
	m1->matrix = (double**) calloc(m1->rows, sizeof(double*));

	for(j = 0; j < m1->rows; j++){
		m1->matrix[j] = (double*) calloc(m1->columns, sizeof(double));
	}
}

/*
   Funcao para desalocar uma matriz
 */
void deallocateMatrix(matrix *m1){
	// Indices para percorrer a matriz
	int i, j;

	// Desalocando as linhas e as colunas
	for(j = 0; j < m1->rows; j++){
		free(m1->matrix[j]);
	}

	free(m1->matrix);

	m1->columns = 0;
	m1->rows = 0;
}

/*
   Funcao que calcula o valor de x em uma iteracao
 */
double calculateValue(int row){
	int j;
	double result = 0;

	// Percorrendo as colunas e calculando o valor
	for(j = 0; j < mA.columns; j++){
		result -= (double) mA.matrix[row][j]*xp[j];
	}
	result += (double) mB.matrix[row][0];

	return result;
}

/*
   Função para ler os parametros de entrada
 */
void readInput(FILE *file){
	int i,j;

	// Le os parametros iniciais
	fscanf(file,"%d",&(J_ORDER));
	fscanf(file,"%d",&(J_ROW_TEST));
	fscanf(file,"%lf",&(J_ERROR));
	fscanf(file,"%d",&(J_IT_MAX));

	// Aloca as matrizes
	allocateMatrix(&(mA), J_ORDER, J_ORDER);
	allocateMatrix(&(mB), 1, J_ORDER);

	// Le a matriz A
	for(i = 0; i < J_ORDER; i++){
		for(j = 0; j < J_ORDER; j++){
			fscanf(file, "%lf", &(mA.matrix[i][j]));
		}
	}

	// Le a matriz B
	for(i = 0; i < J_ORDER; i++){
		fscanf(file,"%lf", &(mB.matrix[i][0]));
	}    
}

/*
   Convertendo a matriz para o formato necessario no Jacobi-Richardson.
   Diagonal principal nula, e todos os outros elementos divididos pela diagonal principal
 */
void prepareMatrix(matrix *m1, matrix *m2){
	// Indices para percorrer a matriz
	int i, j;
	double diag;

	// Percorrendo a matriz
	for(i = 0; i < m1->rows; i++){
		// Diagonal
		diag = m1->matrix[i][i];

		for(j = 0; j < m1->columns; j++){
			// Dividindo os elementos pelo elemento da diagonal principal da matriz
			m1->matrix[i][j] /= diag;
		}
		// Calculando o valor para a matriz de resultados
		m2->matrix[i][0] /= diag;

		// Zerando o valor do diagonal principal
		m1->matrix[i][i] = 0;
	}
}

/*
   Calcula o Erro Absoluto
 */
double absError(double a, double b){
	return fabs(a-b) / fabs(a);
}

/*
   Funcao que imprime matrizes
 */
void printMatrix(matrix *m1){
	int i, j;

	printf("MATRIX BEGIN:\n");
	for(i = 0; i < m1->rows; i++) {
		for(j = 0; j < m1->columns; j++) {
			printf("%lf ", m1->matrix[i][j]);
		}
		printf("\n");
	}
	printf("MATRIX END\n");
}

/*
   Metodo Jacobi Richardson 
 */
void jacobiRichardson(char* file){
	double *testRow, result;
	double max_error;
	int iteration, i;

	struct timespec start, finish;
	double elapsed_temp;

	// Abrindo o arquivo 
	FILE *f = fopen(file, "r");

	// Lendo os dados do arquivo
	readInput(f);
	
	// Alocando memoria para a coluna de teste
	testRow = (double*) calloc(J_ORDER + 1, sizeof(double));

	// Salvando a coluna de testes para posterior avaliacao dos resultados
	for(i = 0; i < J_ORDER; i++){
		testRow[i] = mA.matrix[J_ROW_TEST][i];
	}
	testRow[i] = mB.matrix[J_ROW_TEST][0];

	// Preparando a matriz no formato Jacobi-Richardson
	prepareMatrix(&(mA), &(mB));

	// Alocando memoria para x 
	x = (double*) calloc(J_ORDER, sizeof(double));
	xp = (double*) calloc(J_ORDER, sizeof(double));
	error = (double*) calloc(J_ORDER, sizeof(double));

	// Calculando o tempo
	clock_gettime(CLOCK_MONOTONIC, &start);

	// Calculando valores
	for (iteration = 0; iteration < J_IT_MAX; iteration++) {
		for(i = 0; i < J_ORDER; i++){

			// Calculando os valores
			x[i] = calculateValue(i);

			// Calculando o valor do erro
			error[i] = absError(x[i],xp[i]);
		}

		// Inicializando o valor de max_error
		max_error = error[0];
		for(i = 0; i < J_ORDER; i++){
			// Atualizando o valor anterior de x
			xp[i] = x[i];

			// Atualizando o valor maximo do erro
			if(max_error < error[i]){
				max_error = error[i];
			}
		}

		// Se o erro esta de acordo com o erro do arquivo de entrada 
		if (max_error < J_ERROR){
			break;
		}
	}

	// Calculando o tempo
	clock_gettime(CLOCK_MONOTONIC, &finish);
	elapsed_temp = (finish.tv_sec - start.tv_sec);
	elapsed_temp += (finish.tv_nsec - start.tv_nsec) / 1000000000.0;

	elapsed += elapsed_temp;
	printf("Iterations: %d\n", iteration);

	result = 0;
	for(i = 0; i < J_ORDER; i++){
		result += testRow[i]*x[i];
	}
	printf("RowTest: %d => [%lf] =? %lf\n", J_ROW_TEST, result, testRow[i]);

	// Liberando os vetores de resultado
	free(x);
	free(xp);
	free(testRow);
	free(error);

	// Desalocando Matrizes
	deallocateMatrix(&(mA));
	deallocateMatrix(&(mB));

	// Fechando os arquivos
	fclose(f);
}

int main(int argc, char **argv){
	int i;

	if(argc == 2) {
		for(i = 0; i < 10; i++){
			printf("Iteration %d of 10\n", i + 1);
			jacobiRichardson(argv[1]);
		}
		elapsed = (double) elapsed/10;
		printf("Average time of 10 calculations: %.2lfs\n", elapsed);
	}
	else{
		printf("Not enough parameters - Missing filename\n");
	}
	return 0;
}
